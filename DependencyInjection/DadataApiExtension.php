<?php

/* 
 *  Copyright 2016 Evgeniy Zhelyazkov <evgeniyzhelyazkov@gmail.com>.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace DadataApiBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

class DadataApiExtension extends Extension implements PrependExtensionInterface
{

    public function load(array $configs, ContainerBuilder $container)
    {        
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs); 
        $loader = new YamlFileLoader(
                $container,
                new FileLocator(__DIR__.'/../Resources/config')
            );
        $loader->load('services.yml'); 
        if(!$container->hasDefinition('dadata.client')){
            return;            
        }
        $definition = $container->getDefinition('dadata.client');
        $definition->addArgument($config['api_key']);
        $definition->addArgument($config['base_uri']);
        if(isset($config['content_type'])){
            $definition->addArgument($config['content_type']);
        }    
    }

    public function prepend(ContainerBuilder $container) 
    {  
         // get all bundles
        $bundles = $container->getParameter('kernel.bundles');
        // determine if DadataApiBundle is registered
        if (!isset($bundles['DadataApiBundle'])) {            
            foreach ($container->getExtensions() as $name => $extension) {
                switch ($name) {
                    case 'dadata_api':                        
                        $container->prependExtensionConfig($name, $extension);
                        break;
                }
            }
        }        
        $configs = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration(new Configuration(), $configs);        
    }

}